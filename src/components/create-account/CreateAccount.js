import React, { useState } from "react";
import TagsInput from "react-tagsinput";
import {
    Alert,
    Container,
    Row,
    Col,
    Button,
    Card,
    CardBody,
    CardFooter,
    Form,
    FormInput,
    FormSelect,
    ListGroupItem
} from "shards-react";
import FormSectionTitle from "../edit-user-profile/FormSectionTitle";

function CreateAccount() {

    const [accountdetails, setAccountdetails] = useState({ userName: '', password: '', firstName: '', lastName: '', IsActive: true, roleId: 0 });
    const [errors, setErrors] = useState({});
    const [roleList, setRoleList] = useState([]);
    const onChange = (e) => {
        setAccountdetails({ ...accountdetails, [e.target.name]: e.target.value });
    }
    const Save = (e) => {
        e.preventDefault();
        let data = { username: accountdetails.userName, password: accountdetails.password, firstname: accountdetails.firstName, lastname: accountdetails.lastName, roleid: accountdetails.roleId, isactive: accountdetails.IsActive };
        debugger
    };
    return (
        <div>
            <Container fluid className="px-0">
                <Alert theme="success" className="mb-0">
                    Ole! Your profile has been successfully updated!
          </Alert>
            </Container>
            <Container fluid className="main-content-container px-4">
                <Row>
                    <Col lg="8" className="mx-auto mt-4">
                        <Card small className="edit-user-details mb-4">

                            <CardBody className="p-0">
                                {/* Form Section Title :: General */}
                                <Form className="py-6" >
                                    <FormSectionTitle
                                        title="Create Account"
                                        description=""
                                    /><hr />

                                    <Row form className="mx-4">
                                        <Col lg="12">
                                            <Row form>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="firstName">First Name</label>
                                                    <FormInput
                                                        type="text"
                                                        name="firstName"
                                                        value={accountdetails.firstName}
                                                        onChange={onChange}
                                                        placeholder="First Name"
                                                    />
                                                </Col>

                                                {/* Last Name */}
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="lastName">Last Name</label>
                                                    <FormInput
                                                        type="text"
                                                        name="lastName"
                                                        value={accountdetails.lastName}
                                                        onChange={accountdetails.onChange}
                                                        placeholder="Last Name"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="lastName">User Name</label>
                                                    <FormInput
                                                        type="text"
                                                        name="userName"
                                                        value={accountdetails.userName}
                                                        onChange={onChange}
                                                        placeholder="User Name"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="lastName">Password</label>
                                                    <FormInput
                                                        type="text"
                                                        name="password"
                                                        value={accountdetails.password}
                                                        onChange={onChange}
                                                        placeholder="User Name"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="lastName">Role</label>
                                                    <FormSelect name="roleId" onChange={accountdetails.onChange}>
                                                        <option>Select an Option</option>
                                                        <option value="0">Admin.</option>
                                                        <option value="1">Super Admin.</option>
                                                    </FormSelect>
                                                </Col>

                                            </Row>
                                        </Col>
                                    </Row>

                                </Form>
                            </CardBody>

                            <ListGroupItem className="d-flex px-2">
                                <Button size="md" theme="accent" className="d-table mr-6" onClick={Save}>Save</Button>&nbsp;
                                <Button size="md" theme="accent" className="d-table mr-6" >Cancel</Button>&nbsp;
                                <Button size="md" theme="accent" className="d-table mr-6" >List</Button>
                            </ListGroupItem>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default CreateAccount;
