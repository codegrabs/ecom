import React, { useState } from "react";
import TagsInput from "react-tagsinput";
import {
    Alert,
    Container,
    Row,
    Col,
    Button,
    Card,
    CardBody,
    CardFooter,
    Form,
    FormInput,
    FormSelect,
    ListGroupItem,
    FormTextarea
} from "shards-react";
import FormSectionTitle from "../edit-user-profile/FormSectionTitle";
import ReactQuill from "react-quill";

function StaticPages() {

    const [cms, setCms] = useState({ name: '', url: '', language: '', meta_tag: '', meta_title: '', meta_keyword: '', meta_description: '', cmscontent: '' });
    const [errors, setErrors] = useState({});
    const [roleList, setRoleList] = useState([]);
    const _quillModules = {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],

            [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction

            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],
            ['link', 'image'],          // image or ling ser
            ['clean']                                         // remove formatting button
        ]
    }
    const onChange = (e) => {
        setCms({ ...cms, [e.target.name]: e.target.value });
    }
    const onEditorChange = (e) => {
        setCms({ ...cms, 'cmscontent': e });
    }
    const Save = (e) => {
        e.preventDefault();
        let data = { name: cms.name, url: cms.url, language: cms.language, meta_tag: cms.meta_tag, meta_title: cms.meta_title, meta_keyword: cms.meta_keyword, meta_description: cms.meta_description, cmscontent: '', };
        debugger
    };
    return (
        <div>
            <Container fluid className="px-0">
                <Alert theme="success" className="mb-0">
                    Ole! Your profile has been successfully updated!
          </Alert>
            </Container>
            <Container fluid className="main-content-container px-4">
                <Row>
                    <Col lg="8" className="mx-auto mt-4">
                        <Card small className="edit-user-details mb-4">

                            <CardBody className="p-0">
                                {/* Form Section Title :: General */}
                                <Form className="py-6" >
                                    <FormSectionTitle
                                        title="Create Account"
                                        description=""
                                    /><hr />

                                    <Row form className="mx-4">
                                        <Col lg="12">
                                            <Row form>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="language">Language</label>
                                                    <FormSelect name="language" onChange={onChange}>
                                                        <option>Select an Option</option>
                                                        <option value="english">English</option>
                                                        <option value="germany">Germany</option>
                                                    </FormSelect>
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="name">Name</label>
                                                    <FormInput
                                                        type="text"
                                                        name="name"
                                                        value={cms.name}
                                                        onChange={onChange}
                                                        placeholder="Name"
                                                    />
                                                </Col>

                                                {/* Last Name */}
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="url">Page Url</label>
                                                    <FormInput
                                                        type="text"
                                                        name="url"
                                                        value={cms.url}
                                                        onChange={onChange}
                                                        placeholder="Page Url"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="meta_title">Meta Title</label>
                                                    <FormInput
                                                        type="text"
                                                        name="meta_title"
                                                        value={cms.meta_title}
                                                        onChange={onChange}
                                                        placeholder="Meta Title"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="meta_tag">Meta Tag</label>
                                                    <FormInput
                                                        type="text"
                                                        name="meta_tag"
                                                        value={cms.meta_tag}
                                                        onChange={onChange}
                                                        placeholder="Meta Tag"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="meta_tag">Meta Keyword</label>
                                                    <FormInput
                                                        type="text"
                                                        name="meta_keyword"
                                                        value={cms.meta_keyword}
                                                        onChange={onChange}
                                                        placeholder="Meta Keyword"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="meta_tag">Meta Description</label>
                                                    <FormTextarea
                                                        type="text"
                                                        name="meta_description"
                                                        value={cms.meta_description}
                                                        onChange={onChange}
                                                        placeholder="Meta Description"
                                                        rows="3"
                                                    />
                                                </Col>


                                                <Col md="12" className="form-group">
                                                    <label htmlFor="cmscontent">Content</label>
                                                    <ReactQuill
                                                        name="cmscontent"
                                                        value={cms.cmscontent}
                                                        onChange={onEditorChange}
                                                        className="add-new-post__editor mb-1"
                                                        theme="snow"
                                                        modules={_quillModules}
                                                    />
                                                </Col>

                                            </Row>
                                        </Col>
                                    </Row>

                                </Form>
                            </CardBody>

                            <ListGroupItem className="d-flex px-2">
                                <Button size="md" theme="accent" className="d-table mr-6" onClick={Save}>Save</Button>&nbsp;
                                <Button size="md" theme="accent" className="d-table mr-6" >Cancel</Button>&nbsp;
                                <Button size="md" theme="accent" className="d-table mr-6" >List</Button>
                            </ListGroupItem>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default StaticPages;
