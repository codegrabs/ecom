/* eslint jsx-a11y/anchor-is-valid: 0 */

import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  FormInput,
  Button,
  FormFeedback
} from "shards-react";

function Login() {
  const [user, setUser] = useState({ username: '', password: '' });
  const [errors, setErrors] = useState(false);

  const onChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  }
  const Save = (e) => {
    e.preventDefault();
    let data = { username: user.username, password: user.password };

    let lastAtPos = data.username.lastIndexOf('@');
    let lastDotPos = data.username.lastIndexOf('.');
    debugger;
    if (data.username == "" && data.password == "") {
      setErrors(true);
    }
    else if (!(lastAtPos < lastDotPos && lastAtPos > 0 && data.username.indexOf('@@') == -1 && lastDotPos > 2 && (data.username.length - lastDotPos) > 2)) {
      setErrors(true);
    }
    else{
      setErrors(false);
    }

    if (!errors) {
      alert('user name :' + user.username + ' , password :' + user.password)
    }

  }

  return (
    <Container fluid className="main-content-container h-100 px-4">
      <Row noGutters className="h-100">
        <Col lg="3" md="5" className="auth-form mx-auto my-auto">
          <Card>
            <CardBody>
              {/* Logo */}
              <img
                className="auth-form__logo d-table mx-auto mb-3"
                src={require("../images/shards-dashboards-logo.svg")}
                alt="Shards Dashboards - Login Template"
              />

              {/* Title */}
              <h5 className="auth-form__title text-center mb-4">
                Login
            </h5>

              {/* Form Fields */}
              <Form>
                <FormGroup>
                  <label htmlFor="exampleInputEmail1">Email address</label>
                  <FormInput
                    type="email"
                    id="exampleInputEmail1"
                    name="username"
                    placeholder="Enter email"
                    autoComplete="email"
                    value={user.username}
                    onChange={onChange}

                  />
                  {errors  &&
                    <FormFeedback>The username is taken.</FormFeedback>
                  }

                </FormGroup>
                <FormGroup>
                  <label htmlFor="exampleInputPassword1">Password</label>
                  <FormInput
                    type="password"
                    id="exampleInputPassword1"
                    placeholder="Password"
                    name="password"
                    autoComplete="current-password"
                    value={user.password}
                    onChange={onChange}
                  />
                </FormGroup>
                {/* <FormGroup>
                <FormCheckbox>Remember me for 30 days.</FormCheckbox>
              </FormGroup> */}
                <Button
                  pill
                  theme="accent"
                  className="d-table mx-auto"
                  type="submit"
                  onClick={Save}
                >
                  LogIn
              </Button>
              </Form>
            </CardBody>


          </Card>

          {/* Meta Details */}
          <div className="auth-form__meta d-flex mt-4">
            {/* <Link to="/forgot-password">Forgot your password?</Link> */}
            {/* <Link to="/register" className="ml-auto">
            Create a new account?
          </Link> */}
          </div>
        </Col>
      </Row>
    </Container>
  )
}

export default Login;
