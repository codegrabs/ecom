import React, { useState } from "react";
import TagsInput from "react-tagsinput";
import {
    Alert,
    Container,
    Row,
    Col,
    Button,
    Card,
    CardBody,
    CardFooter,
    Form,
    FormInput,
    FormSelect,
    ListGroupItem
} from "shards-react";
import FormSectionTitle from "../edit-user-profile/FormSectionTitle";

function DyanmicMessage() {

    const [msg, setMsg] = useState({ key: '', msg_en: '', msg_nl: '', msg_de: '' });
    const [errors, setErrors] = useState({});
    const [roleList, setRoleList] = useState([]);
    const onChange = (e) => {
        setMsg({ ...msg, [e.target.name]: e.target.value });
    }
    const Save = (e) => {
        e.preventDefault();
        let data = { key: msg.key, msg_en: msg.msg_en, msg_de: msg.msg_de};
        
    };
    return (
        <div>
            <Container fluid className="px-0">
                <Alert theme="success" className="mb-0">
                    Ole! Your profile has been successfully updated!
          </Alert>
            </Container>
            <Container fluid className="main-content-container px-4">
                <Row>
                    <Col lg="8" className="mx-auto mt-4">
                        <Card small className="edit-user-details mb-4">

                            <CardBody className="p-0">
                                {/* Form Section Title :: General */}
                                <Form className="py-6" >
                                    <FormSectionTitle
                                        title="Dynamic Message"
                                        description=""
                                    /><hr />

                                    <Row form className="mx-4">
                                        <Col lg="12">
                                            <Row form>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="key">Key</label>
                                                    <FormInput
                                                        type="text"
                                                        name="key"
                                                        value={msg.key}
                                                        onChange={onChange}
                                                        placeholder="Key"
                                                        maxlength="10"
                                                    />
                                                </Col>

                                                {/* Last Name */}
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="msg_en">English</label>
                                                    <FormInput
                                                        type="text"
                                                        name="msg_en"
                                                        value={msg.msg_en}
                                                        onChange={onChange}
                                                        placeholder="English Value"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="msg_nl">Dutch</label>
                                                    <FormInput
                                                        type="text"
                                                        name="msg_nl"
                                                        value={msg.msg_nl}
                                                        onChange={onChange}
                                                        placeholder="Netherlands Value"
                                                    />
                                                </Col>
                                                <Col md="6" className="form-group">
                                                    <label htmlFor="msg_de">Dutch</label>
                                                    <FormInput
                                                        type="text"
                                                        name="msg_de"
                                                        value={msg.msg_de}
                                                        onChange={onChange}
                                                        placeholder="Dutch Value"
                                                    />
                                                </Col>

                                            </Row>
                                        </Col>
                                    </Row>

                                </Form>
                            </CardBody>

                            <ListGroupItem className="d-flex px-2">
                                <Button size="md" theme="accent" className="d-table mr-6" onChange={Save}>Save</Button>&nbsp;
                                <Button size="md" theme="accent" className="d-table mr-6" >Cancel</Button>&nbsp;
                                <Button size="md" theme="accent" className="d-table mr-6" >List</Button>
                            </ListGroupItem>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default DyanmicMessage;
